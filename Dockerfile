FROM registry.gitlab.com/sisicmbio/infra/co7_httpd_php_7.2:production  AS production
ARG PDCI_COMMIT_REF_NAME
ARG PDCI_COMMIT_SHA
ARG PDCI_COMMIT_SHORT_SHA
ARG PDCI_COMMIT_TAG

LABEL PDCI_COMMIT_REF_NAME="${PDCI_COMMIT_REF_NAME}"
LABEL PDCI_COMMIT_SHA="${PDCI_COMMIT_SHA}"
LABEL PDCI_COMMIT_SHORT_SHA="${PDCI_COMMIT_SHORT_SHA}"
LABEL PDCI_COMMIT_TAG="${PDCI_COMMIT_TAG}"

ENV APP_HOME /var/www/html/{projeto}
ENV APP_PROJETO "{projeto}"

RUN rm -rf /var/www/html/*
RUN mkdir -p $APP_HOME/
RUN rm -rf $APP_HOME/*

COPY conf/etc/httpd/conf.d/$APP_PROJETO.conf /etc/httpd/conf.d/$APP_PROJETO.conf
ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh
COPY src/  $APP_HOME

COPY src/.env.example $APP_HOME/.env

#RUN sed -i "s|;include_path = \".:/php/includes\"|include_path = \".:/usr/share/pear:/usr/share/php:$APP_HOME/vendor/zendframework/zendframework1/library\"|" /etc/php.ini
RUN sed -i "s|;date.timezone =|date.timezone=\"America/Sao_Paulo\"|" /etc/php.ini

WORKDIR $APP_HOME

RUN composer clearcache && \
    composer install --no-dev --optimize-autoloader

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

EXPOSE 80
VOLUME ["/var/lib/php/session"]

CMD ["/run-httpd.sh"]

FROM registry.gitlab.com/sisicmbio/infra/co7_httpd_php_7.2:testing AS testing

ENV APP_HOME /var/www/html/{projeto}
ENV APP_PROJETO "{projeto}"

COPY conf/etc/httpd/conf.d/$APP_PROJETO.conf /etc/httpd/conf.d/$APP_PROJETO.conf
ENV APP_HOME /var/www/html

WORKDIR $APP_HOME

RUN rm -rf $APP_HOME/*

COPY src/  $APP_HOME

COPY src/.env.example $APP_HOME/.env

WORKDIR $APP_HOME

RUN composer clearcache && \
  composer install && \
  yarn install

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

CMD ["/run-httpd.sh"]
