﻿DO
$do$
BEGIN
  IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'usr_{projeto}') THEN

    create user usr_{projeto} with password 'usr_{projeto}';

  END IF;
  CREATE SCHEMA IF NOT EXISTS {projeto} AUTHORIZATION usr_{projeto} ;
  GRANT ALL ON SCHEMA {projeto} TO postgres;
  GRANT USAGE ON SCHEMA {projeto} TO usr_{projeto};
END
$do$;